<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project_model extends CI_Model{
    private $dbase;

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // metodo para saber se o usuarui esta logado
    public function getSession($session) {
        if($session != "true") {
            session_destroy();
            redirect('/');
        }
    }

    public function login() {
        // Comparação dos dados digitados pelo usuario para efetuar o login,
        // Addslashes é uma foma básica para prevenção de SQL injection;
        $this->db->where('email', addslashes($this->input->post('email')));
        $this->db->where('password', addslashes(md5($this->input->post('password'))));

        $query = $this->db->get('usuario');

        if ($query->num_rows == 1) {
            return true; // RETORNA VERDADEIRO = LOGIN CONCLUIDO
        }
        return $query->result_array();
    }

    public function altera_senha($email, $senha) {
        $this->db->where('email', $email);
        $this->db->set('password', $senha);
        return $this->db->update('usuario');
    }

    public function cria_conta($nome, $email, $senha) {
        // Variabel receberá uma mensagem de cadastro ja existemte quando tentar repetir cadastro de email
        $cadastro = '';
        
        // Busca do email informado, para verificar duplicidade no DB
        $this->db->where('email', addslashes($email));
        $query = $this->db->get('usuario');

        // caso email ja exista no banco, a mensagem é imformada
        if($query->num_rows() > 0) {
            $cadastro = 'Já existe este email cadastrado em nosso sistema';
        }else{
            $data = array(
                'nome'      => $nome,
                'email'     => $email,
                'password'  => md5($senha)
            );
            $cadastro = 'Cadastro concluido!';
           
            $this->db->insert('usuario', $data);  
     
            redirect('/');
        }      
        return $cadastro;  
    }

    public function cadastro_cliente($nome, $cpj, $cnpj, $telefone, $empresa, $email, $cargo) {
        // Variabel receberá uma mensagem de cadastro ja existemte quando tentar repetir cadastro de email
        $cadastro = '';
        
        // Busca do email informado, para verificar duplicidade no DB
        $this->db->where('email', addslashes($email));
        $query = $this->db->get('cliente');

         // caso email ja exista no banco, a mensagem é imformada
        if($query->num_rows() > 0) {
            $cadastro = 'Já existe cadastro deste cliente em nosso sistema';
        }else{
            $data = array(
                'nome'      => $nome,
                'email'     => $email,
                'telefone'  => $telefone,
                'cpf'       => $cpj,
                'cnpj'      => $cnpj,
                'empresa'   => $empresa,
                'cargo'     => $cargo
            );
            $cadastro = 'Cadastro concluido!';
           
            $this->db->insert('cliente', $data);  
     
            redirect('welcome/clientes');
        }      
        return $cadastro;  
    }

    public function cadastro_insert($table, $dados) {          
        $cadastro = $this->db->insert($table, $dados); 
        $last_id = $this->db->insert_id(); 
        return $last_id;  
    }

    public function cadastro_estado($dados) {
        $this->db->insert('Estados', $dados);
    }

    public function insert_levantamento($id, $id_user, $descricao){
        $dados = array('id_cliente' => $id, 'id_usuario' => $id_user, 'descricao' => $descricao);
        $this->db->limit(1);
        $this->db->insert("Levantamento",$dados);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function edita_conta($id_user, $nome = null, $email = null, $password = null) {
        $cadastro = '';
        $data = array();

        // Recebendo todos os dados do usuario
        $query = $this->getDadosUsuarios(addslashes($id_user));
        foreach($query as $info){
            $data_nome = $info['nome'];
            $data_email = $info['email'];
            $data_password = $info['password'];
        };
        
        if($email != null) {
            // Comparacao do email informado para evitar duplicidade de cadastro do mesmo
            $this->db->where('email', addslashes($email));
            $query2 = $this->db->get('usuario');
                      
            if($query2->num_rows() > 0) {
                $cadastro = 'Já existe este email cadastrado em nosso sistema';
            }else{
                $data_email = $email;
            }           
        }
        
        if($nome != null) {
            $data_nome = $nome;
        }
        
        if($password != null) {
            $data_password = md5($password);
        }
      
        if($nome != null || $password != null || $email != null) {
            $cadastro = 'Alteração concluida!';
            $query = $this->db->query("UPDATE usuario SET nome = '$data_nome', email = '$data_email', password = '$data_password' WHERE id_usuario = $id_user "); 
        };
       
        return $cadastro;  
    }

    public function alterar_status($id_luminaria, $data) {
        $this->db->where('id_Luminarias', $id_luminaria);
        $this->db->set($data);
        $this->db->update('Luminarias');
    }

    public function deleta_conta($id) {
        $this->db->where('id_usuario', $id);
        $this->db->delete('usuario');
    }

    public function deleta_luminaria($id) {
        $this->db->where('id_Luminarias', $id);
        $this->db->delete('Luminarias');
    }

    // Retorna os Dados dos usuario para que possa ser feita uma alteração na pagina Prifile
    public function getDadosUsuarios($id_user) {
        $this->db->where('id_usuario', $id_user);
        $query = $this->db->get('usuario');
        return $query->result_array();        
    }

    public function getDadosClientes($id_cliente) {
        $this->db->where('id_cliente', $id_user);
        $query = $this->db->get('cliente');
        return $query->result_array(); 
    }

    public function getTodosUsuarios() {
        $query = $this->db->get('usuario');
        return $query->result_array(); 
    }

    // Retorna a lista de Clientes para que seja efetuado o cadastro do mesmo em um levantamento
    public function getTodosClientes() {
        $query = $this->db->get('cliente');
        return $query->result_array(); 
    }

    // Retorna somente Clientes que possuem Levantamento Cadastrado
    public function GetClientesLevantamento() {
        $this->db->join('Levantamento', 'Levantamento.id_cliente = cliente.id_cliente');
        $query = $this->db->get('cliente');
        
        if($query->num_rows() != 0)
        {
            return $query->result_array();
        }
    }

    // public function alterar_status_levantamento($id, $status) {
    //     $this->db->set('status_confere', $status)
    //     ->where('id_Luminarias',$id)
    //    ->update('Luminarias');
    // }

    // Metodo retorna somente clientes que possuem Levantamento Cadastrado
    public function getSelectLevantamentos() {
        $this->db->select('descricao, cliente.nome, Levantamento.id_levantamento');
        $this->db->from('Levantamento');
        $this->db->join('cliente', 'cliente.id_cliente = Levantamento.id_cliente');
        $query = $this->db->get(); 
        if($query->num_rows() != 0)
        {
            return $query->result_array();
        }
    }

    // Retorna Todos os Levantamentos Cadastracos com Limite para a paginação
    public function getTodosLevantamentos_Condicao($cliente = null, $limit = null) {
        $this->db->select('
        Levantamento.id_levantamento,
        Levantamento.descricao as Descricao_Levantamento,
        Levantamento.imagem,
        cliente.nome as Cliente,
        usuario.nome as Colaborador,
        Blocos.descricao as Descricao_Blocos,
        Pavimentos.descricao as Descricao_Pavimentos,
        Luminarias.descricao as Descricao_Luminarias,
        Luminarias.status,
        Luminarias.id_Luminarias');
        $this->db->from("Levantamento");
        $this->db->join('cliente', 'Levantamento.id_cliente = cliente.id_cliente');
        $this->db->join('usuario', 'Levantamento.id_usuario = usuario.id_usuario');
        $this->db->join('Blocos', 'Levantamento.id_levantamento = Blocos.id_levantamento');
        $this->db->join('Pavimentos', 'Levantamento.id_levantamento = Pavimentos.id_levantamento');
        $this->db->join('Luminarias', 'Levantamento.id_levantamento = Luminarias.id_levantamento');
        if($cliente != null) {
            $this->db->where('cliente.id_cliente', $cliente);
        }        
        $this->db->order_by("cliente.nome", "asc");   
        $this->db->order_by("Blocos.descricao", "asc");
        $this->db->order_by("Pavimentos.descricao", "asc");
        $this->db->order_by("Luminarias.descricao", "asc");
        $this->db->limit(10, $limit);
      
        $query = $this->db->get(); 
        if($query->num_rows() != 0)
        {
            return $query->result_array();
        }
    }

    // Retornando o Mesmo Metodo acima porem sem limit para gerar um
    public function count_getTodosLevantamentos_Condicao($cliente = null) {
        $this->db->select('
        Levantamento.descricao as Descricao_Levantamento,
        Levantamento.imagem,
        cliente.nome as Cliente,
        usuario.nome as Colaborador,
        Blocos.descricao as Descricao_Blocos,
        Pavimentos.descricao as Descricao_Pavimentos,
        Luminarias.descricao as Descricao_Luminarias,
        Luminarias.id_Luminarias');
        $this->db->from("Levantamento");
        $this->db->join('cliente', 'Levantamento.id_cliente = cliente.id_cliente');
        $this->db->join('usuario', 'Levantamento.id_usuario = usuario.id_usuario');
        $this->db->join('Blocos', 'Levantamento.id_levantamento = Blocos.id_levantamento');
        $this->db->join('Pavimentos', 'Levantamento.id_levantamento = Pavimentos.id_levantamento');
        $this->db->join('Luminarias', 'Levantamento.id_levantamento = Luminarias.id_levantamento');
        if($cliente != null) {
            $this->db->where('cliente.id_cliente', $cliente);
        } 
        $this->db->order_by("cliente.nome", "asc");   
        $this->db->order_by("Blocos.descricao", "asc");
        $this->db->order_by("Pavimentos.descricao", "asc");
        $this->db->order_by("Luminarias.descricao", "asc");
      
        $query = $this->db->get(); 
        if($query->num_rows() != 0)
        {
            return $query->num_rows();
        }
    }

    public function get_status_positivo() {
        $query = $this->db->query("SELECT count(Estados.id_estados) as count_positivo,
                                 cliente.nome as cliente
                            FROM Estados 
                        inner join Levantamento
                            on Levantamento.id_levantamento = Estados.id_levantamento
                        inner join cliente
                            on cliente.id_cliente = Levantamento.id_cliente
                        where Estados.descricao = 'Status bom'
                            group by cliente.nome");
        
        return $query->result_array();
    }

}

