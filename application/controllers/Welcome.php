<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() {
        parent::__construct();
        // carreganho o model Project_model e setando um apelido de PjModel
		$this->load->model('Project_model', 'PjModel');
		
        $this->load->helper('url');
    }
	
	public function index() {
		
		// carregando os dados do formulario de login
		if(isset($_POST["email"])) {
			$email = addslashes($_POST["email"]);
			$password = addslashes(md5($_POST["password"]));

			$usuarios_info = $this->PjModel->login();
			// a7cdfbea90c786f3490ef43700d62da3
			foreach($usuarios_info as $info) {
				$userLogin 				= $info["email"];
				$userPassword 			= $info["password"];
				$_SESSION['id_user'] 	= $info['id_usuario'];
				$_SESSION['nome']		= $info['nome'];
				$_SESSION['data'] 		= Date('m/Y');

				if($email == $userLogin && $password == $userPassword) {
					$_SESSION["logado"] = "true";
					redirect("welcome/inicio/1");
				}else{
					$_SESSION["logado"] = "false";
				}
			};  
		};
		$this->load->view('login');
	}

	public function inicio() {
		$cliente = $this->input->post('id_cliente');
		// echo $cliente;

		$uri = explode('/', isset($_SERVER['REQUEST_URI']) ? preg_replace('/^\//', '', $_SERVER['REQUEST_URI'], 1) : '');

        $pagina = $uri[2];
        $dados['pagina'] = $pagina;
        //echo $categoria;
        
        // --------------------- PAGINAÇÂO --------------------
        if(isset($uri[2])) {
            $pg = $uri[2];
        }else{
            $pg = 0;
        }

        $pagina= ($pg - 1) * 8;
        if( $pagina < 0) {
            $pagina = 0;
        }

		$dados['pHome']             = $pagina;
		
		
        $dados['total_registros']   	= 8;
		$dados['dados_levantamento'] 	= $this->PjModel->getTodosLevantamentos_Condicao($cliente ,$pagina);
		$dados['count']             	= $this->PjModel->count_getTodosLevantamentos_Condicao($cliente);
		$dados['paginas']           	= ceil($dados['count'] / 8); 
	
		// echo '<br><br><br><br><br><br>';
		// var_dump($dados['dados_levantamento'] );
		
		$dados['dados_status'] = $this->PjModel->get_status_positivo();
		
		$dados['dados_clientes'] = $this->PjModel->GetClientesLevantamento();
		$this->load->view('Gestao/template/inicio', $dados);
	}

	public function CreateAccount() {
		$email 		= $this->input->post('email');
		$nome 		= $this->input->post('nome');
		$password	= $this->input->post('password');

		$success = '';
		if(isset($email)) {
			// Funcão que chama o insert para a criação da conta
			$success = $this->PjModel->cria_conta($nome, $email, $password);		
		}
		// Mensagem que vem no metodo de cadastro, 
		$dados['Mensagem'] = $success;
	
		$this->load->view('create_account', $dados);	
	}

	public function ForgotAccount() {
		$this->load->view('forgot_account');
	}

	public function  LANDING_PAGE() {
		$dados['viewName'] = 'LANDING_PAGE';
		$this->load->view('Template', $dados);
	}

	public function enviaEmail() {
		$this->PjModel->getSession($_SESSION["logado"]);
		// gerando uma chave para substituir a senha antiga
		$chave = mt_rand(5, 15). date('m') . date('s');
		// adcionando a nova chama na variavel $_POST que sera carregada e enviada por email
		$_POST['new_senha'] = $chave;
		// alterando a senha antiga pela chave gerada
		$this->PjModel->altera_senha($_POST['email'], md5($chave));
		// carregando a Library que envia o email
		$this->load->library('Sendmail');
		// uma mensagem gerada apos o click do botao de enviar email
        $dados['email_enviado'] = $_SESSION['enviado'];
    
		$dados['viewName'] = 'forgot_account';
		$this->load->view('forgot_account', $dados);

		sleep(3);
		redirect("welcome/index");
	}

	public function Quem_somos() {
		$dados['viewName'] = 'quemsomos';
		$this->load->view('Template', $dados);
	}
	public function Produtos_servicos() {
		$dados['viewName'] = 'Produtos_servicos';
		$this->load->view('Template', $dados);
	}

	public function profile() {
		$this->PjModel->getSession($_SESSION["logado"]);
		$email 		= $this->input->post('email');
		$nome 		= $this->input->post('nome');
		$password	= $this->input->post('password');

		$id_user_delete = $this->input->post('id_user_delete');

		// teste de AJAX
		$dados_id = $this->input->post('id');
		
		if(isset($id_user_delete)) {
			$this->PjModel->deleta_conta($id_user_delete);
		}

	
		if(isset($_POST) && $_POST != "") {
			// Funcão que chama o insert para a criação da conta
			$success = $this->PjModel->edita_conta($_SESSION['id_user'] , $nome, $email, $password);
		}
		
		// Mensagem que vem no metodo de cadastro, 
		$dados['Mensagem'] = $success;

		// Pega os dados do Usuario logado
		$dados['dados_usuario_logado'] = $this->PjModel->getDadosUsuarios($_SESSION['id_user']);

		// Pega os dados de todos os uduarios
		$dados['dados_usuarios_geral'] = $this->PjModel->getTodosUsuarios();
		
		// Count de usuarios para informaçoes adicionais na pagian
		$count = 0;
		foreach($dados['dados_usuarios_geral'] as $info) {
			$count += 1;
		}
		$dados['count_users'] = $count;

		$this->load->view('Gestao/template/profile', $dados);
	}

	public function clientes() {
		$this->PjModel->getSession($_SESSION["logado"]);

		$nome = $this->input->post('nome');
		$cpj = $this->input->post('cpf');
		$cnpj = $this->input->post('cnpj');
		$empresa = $this->input->post('empresa');
		$telefone = $this->input->post('telefone');
		$email = $this->input->post('email');
		$cargo = $this->input->post('cargo');

		$btn_post = $this->input->post('btn_post');

		if(isset($btn_post)) {
			$this->PjModel->cadastro_cliente($nome, $cpj, $cnpj, $telefone, $empresa, $email, $cargo);
		}

		$dados['dados_clientes'] = $this->PjModel->getTodosClientes();

		$this->load->view('Gestao/template/clientes', $dados);
	}

	public function insert_levantamento() {
		$id_cliente = $this->input->post('id_cliente');
		$descricao = $this->input->post('descricao');
		$id_levantamento = $this->PjModel->insert_levantamento($id_cliente, $_SESSION['id_user'], $descricao);
		redirect('welcome/levantamentos/1');		
	}

	public function insert_blocos() {
		$id_levantamento = $this->input->post('id_levantamento');

		if(isset($id_levantamento)) {
			
			$count_descricao =  count($_POST);
			for ($i=0; $i < ($count_descricao - 1); $i++) { 
			
				$dados = array(
					'descricao' => $_POST['descricao'.$i],
					'id_levantamento' => $id_levantamento
				);
				$dados = $this->PjModel->cadastro_insert('Blocos', $dados);
			}				
		}
		redirect('welcome/levantamentos/1');
		
	}

	public function insert_pavimentos() {
		$id_levantamento = $this->input->post('id_levantamento');


		if(isset($id_levantamento)) {
			
			$count_descricao =  count($_POST);
			for ($i=0; $i < ($count_descricao - 1); $i++) { 
			
				$dados = array(
					'descricao' => $_POST['descricao'.$i],
					'id_levantamento' => $id_levantamento
				);
				$this->PjModel->cadastro_insert('Pavimentos', $dados);
			}				
		}
		redirect('welcome/levantamentos/1');
	}

	public function insert_luminarias() {
		$id_levantamento = $this->input->post('id_levantamento');

		if(isset($id_levantamento)) {
			
			$count_descricao =  count($_POST);
			for ($i=0; $i < ($count_descricao - 1); $i++) { 
			
				$dados = array(
					'descricao' => $_POST['descricao'.$i],
					'id_levantamento' => $id_levantamento
				);
				$id_Luminarias = $this->PjModel->cadastro_insert('Luminarias', $dados);

				$dados_status = array(
					'id_levantamento' => $id_levantamento,
					'id_Luminarias'=> $id_Luminarias
				);
				$this->PjModel->cadastro_insert('Estados', $dados_status);
			}				
		}
		redirect('welcome/levantamentos/1');
	}

	public function levantamentos() {
		$cliente = $this->input->post('id_cliente');

		$btn_status_ok = $this->input->post('btn_status_ok');
		$btn_status_fail = $this->input->post('btn_status_fail');
		$btn_status_cancel = $this->input->post('btn_status_cancel');

		if(isset($btn_status_ok)) {
			$var = explode('_',$btn_status_ok);
			$id_levantamento = $var[0];
			$id_luminaria = $var[1];
			$dados = array("descricao" => 'Status bom', 'id_levantamento' => $id_levantamento, 'id_Luminarias' => $id_luminaria);
			$this->PjModel->cadastro_estado($dados);

			$this->PjModel->alterar_status($id_luminaria, $data = array('id_Luminarias' => $id_luminaria, 'status' => 'Status bom'));
		}	

		if(isset($btn_status_fail)) {
			$var = explode('_',$btn_status_fail);
			$id_levantamento = $var[0];
			$id_luminaria = $var[1];
			$dados = array("descricao" => 'Status ruim', 'id_levantamento' => $id_levantamento, 'id_Luminarias' => $id_luminaria);
			$this->PjModel->cadastro_estado($dados);

			$this->PjModel->alterar_status($id_luminaria, $data = array('id_Luminarias' => $id_luminaria, 'status' => 'Status ruim'));
		}

		if(isset($btn_status_cancel)) {			
			$query = $this->PjModel->deleta_luminaria($btn_status_cancel);
		}

		$uri = explode('/', isset($_SERVER['REQUEST_URI']) ? preg_replace('/^\//', '', $_SERVER['REQUEST_URI'], 1) : '');

        $pagina = $uri[2];
        $dados['pagina'] = $pagina;
        //echo $categoria;
        
        // ---------------------  INICIO PAGINAÇÂO --------------------
        if(isset($uri[2])) {
            $pg = $uri[2];
        }else{
            $pg = 0;
        }

        $pagina= ($pg - 1) * 8;
        if( $pagina < 0) {
            $pagina = 0;
        }

		$dados['pHome'] = $pagina;
	    $dados['total_registros']   	= 8;
		$dados['dados_levantamento'] 	= $this->PjModel->getTodosLevantamentos_Condicao($cliente ,$pagina);
		$dados['count']             	= $this->PjModel->count_getTodosLevantamentos_Condicao($cliente);
		$dados['paginas']           	= ceil($dados['count'] / 8); 
		 // ---------------------  FIM PAGINAÇÂO --------------------

		// Metodo retorna somente clientes que possuem Levantamento Cadastrado
		$dados['dados_selecao_cliente'] = $this->PjModel->getSelectLevantamentos();
		
    	// Retorna a lista de Clientes para que seja efetuado o cadastro do mesmo em um levantamento
		$dados['dados_clientes'] = $this->PjModel->getTodosClientes();
		
		// Retorna todos os Levantamentos
		$dados['dados_inseridos'] = $this->PjModel->getTodosLevantamentos_Condicao();
		$this->load->view('Gestao/template/levantamentos', $dados);
	}
}
