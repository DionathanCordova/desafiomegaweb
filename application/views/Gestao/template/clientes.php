<!doctype html>
<html lang="en">

<head>
	<title>Clientes</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/vendor/bootstrap/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/vendor/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/vendor/linearicons/style.css')?>">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/css/main.css')?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/css/demo.css')?>">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('Assets/gestao/img/apple-icon.png')?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('Assets/gestao/img/favicon.png')?>">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="index.html"><img src="<?php echo base_url('Assets/gestao/img/logo-dark.png')?>" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">					
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url('Assets/gestao/img/user.png')?>" class="img-circle" alt="Avatar"> <span><?php echo $_SESSION['nome']?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo base_url('welcome/profile')?>"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
								<li><a href="<?php echo base_url('/')?>"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav><br>
					<ul class="nav">
						<li><a href="<?php echo base_url('welcome/inicio/1')?>" class=""><i class="lnr lnr-home"></i> <span>Home</span></a></li>
						<li><a href="<?php echo base_url('welcome/profile')?>" class="" ><i class="lnr lnr-file-empty"></i> <span>Profile</span></a></li>
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed active"><i class="lnr lnr-file-empty"></i> <span>Cadastros Gerais</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="<?php echo base_url('welcome/clientes')?>" class="">Clientes</a></li>
									<li><a href="<?php echo base_url('welcome/levantamentos/1')?>" class="">Levantamentos</a></li>
								</ul>
							</div>
						</li>
						<li><a href="<?php echo base_url('welcome/LANDING_PAGE')?>" class="" ><i class="lnr lnr-linearicons"></i> <span> LANDING PAGE</span></a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<h3 class="page-title">Tables</h3>
					<div class="row">
						<div class="col-md-4">
							<!-- BASIC TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Cadastro de Clientes</h3>
								</div>
								<form action="<?php echo base_url('welcome/clientes')?>" method='POST'>
									<div class='form-group container-fluid'>
										<label for="nome">Nome</label>
										<input type="text" name='nome' class='form-control'>

										<label for="nome">Cpf</label>
										<input type="text" name='cpf' class='form-control'>

										<label for="nome">Cnpj</label>
										<input type="text" name='cnpj' class='form-control'>

										<label for="nome">Telefone</label>
										<input type="text" name='telefone' class='form-control'>

										<label for="nome">Empresa</label>
										<input type="text" name='empresa' class='form-control'>

										<label for="nome">Cargo</label>
										<input type="text" name='cargo' class='form-control'>

										<label for="nome">Email</label>
										<input type="text" name='email' class='form-control'><br>

										<button class='btn btn-default' name='btn_post'>Enviar</button>
									</div>
								</form>

							</div>
							<!-- END BASIC TABLE -->
						</div>
						<div class="col-md-8">
							<!-- TABLE NO PADDING -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Tabela de Dados dos Clientes</h3>
								</div>
								<div class="panel-body no-padding">
									<table class="table">
										<thead>
											<tr >
												<th class='text-center'>Nome</th>
												<th class='text-center'>Cpf</th>
												<th class='text-center'>Cnpj</th>
												<th class='text-center'>Telefone</th>
												<th class='text-center'>Empresa</th>
												<th class='text-center'>Cargo</th>
												<th class='text-center'>Email</th>
											</tr>
										</thead>
										<tbody>
										<?php foreach($dados_clientes as $info):?>
											<tr>
												<th class='text-center'><?php echo $info['nome']?></th>
												<th class='text-center'><?php echo $info['cpf']?></th>
												<th class='text-center'><?php echo $info['cnpj']?></th>
												<th class='text-center'><?php echo $info['telefone']?></th>
												<th class='text-center'><?php echo $info['empresa']?></th>
												<th class='text-center'><?php echo $info['cargo']?></th>
												<th class='text-center'><?php echo $info['email']?></th>
											</tr>
										<?php endforeach?>
										</tbody>
									</table>
								</div>
							</div>
							<!-- END TABLE NO PADDING -->
						</div>
					</div>
					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">&copy; 2017 <a href="https://www.themeineed.com" target="_blank">Theme I Need</a>. All Rights Reserved.</p>
			</div>
		</footer>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<?php echo base_url('Assets/gestao/vendor/jquery/jquery.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/vendor/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/scripts/klorofil-common.js')?>"></script>
</body>

</html>
