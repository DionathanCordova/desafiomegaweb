-- MySQL Script generated by MySQL Workbench
-- sáb 25 ago 2018 04:34:37 -03
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema FieldDB
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema FieldDB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `FieldDB` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
USE `FieldDB` ;

-- -----------------------------------------------------
-- Table `FieldDB`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FieldDB`.`cliente` (
  `id_cliente` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(80) NOT NULL,
  `cpf` VARCHAR(15) NOT NULL,
  `telefone` VARCHAR(20) NOT NULL,
  `empresa` VARCHAR(60) NOT NULL,
  `cnpj` VARCHAR(45) NOT NULL,
  `cargo` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_cliente`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `FieldDB`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FieldDB`.`usuario` (
  `id_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(80) NOT NULL,
  `password` VARCHAR(80) NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_usuario`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `FieldDB`.`Levantamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FieldDB`.`Levantamento` (
  `id_levantamento` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NOT NULL,
  `imagem` VARCHAR(80) NULL DEFAULT NULL,
  `id_cliente` INT(11) NOT NULL,
  `id_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`id_levantamento`),
  INDEX `fk_Levantamento_cliente1_idx` (`id_cliente` ASC),
  INDEX `fk_Levantamento_usuario1_idx` (`id_usuario` ASC),
  CONSTRAINT `fk_Levantamento_cliente1`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `FieldDB`.`cliente` (`id_cliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Levantamento_usuario1`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `FieldDB`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `FieldDB`.`Blocos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FieldDB`.`Blocos` (
  `id_blocos` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NOT NULL,
  `id_levantamento` INT(11) NOT NULL,
  PRIMARY KEY (`id_blocos`),
  INDEX `fk_Blocos_Levantamento1_idx` (`id_levantamento` ASC),
  CONSTRAINT `fk_Blocos_Levantamento1`
    FOREIGN KEY (`id_levantamento`)
    REFERENCES `FieldDB`.`Levantamento` (`id_levantamento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 88
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `FieldDB`.`Luminarias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FieldDB`.`Luminarias` (
  `id_Luminarias` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NOT NULL,
  `id_levantamento` INT(11) NOT NULL,
  `status` VARCHAR(12) NULL DEFAULT NULL,
  PRIMARY KEY (`id_Luminarias`),
  INDEX `fk_Luminarias_Levantamento1_idx` (`id_levantamento` ASC),
  CONSTRAINT `fk_Luminarias_Levantamento1`
    FOREIGN KEY (`id_levantamento`)
    REFERENCES `FieldDB`.`Levantamento` (`id_levantamento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 25
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `FieldDB`.`Estados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FieldDB`.`Estados` (
  `id_estados` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NOT NULL DEFAULT 'Avaliar',
  `id_levantamento` INT(11) NOT NULL,
  `id_Luminarias` INT(11) NOT NULL,
  PRIMARY KEY (`id_estados`),
  INDEX `fk_Estados_Levantamento1_idx` (`id_levantamento` ASC),
  INDEX `fk_Estados_Luminarias1_idx` (`id_Luminarias` ASC),
  CONSTRAINT `fk_Estados_Levantamento1`
    FOREIGN KEY (`id_levantamento`)
    REFERENCES `FieldDB`.`Levantamento` (`id_levantamento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Estados_Luminarias1`
    FOREIGN KEY (`id_Luminarias`)
    REFERENCES `FieldDB`.`Luminarias` (`id_Luminarias`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 65
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `FieldDB`.`Pavimentos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `FieldDB`.`Pavimentos` (
  `id_pavimentos` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NOT NULL,
  `id_levantamento` INT(11) NOT NULL,
  PRIMARY KEY (`id_pavimentos`),
  INDEX `fk_Pavimentos_Levantamento1_idx` (`id_levantamento` ASC),
  CONSTRAINT `fk_Pavimentos_Levantamento1`
    FOREIGN KEY (`id_levantamento`)
    REFERENCES `FieldDB`.`Levantamento` (`id_levantamento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 27
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
