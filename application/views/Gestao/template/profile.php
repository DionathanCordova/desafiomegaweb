<!doctype html>
<html lang="en">

<head>
	<title>Profile</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/vendor/bootstrap/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/vendor/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/vendor/linearicons/style.css')?>">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/css/main.css')?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/css/demo.css')?>">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('Assets/gestao/img/apple-icon.png')?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('Assets/gestao/img/favicon.png')?>">
	<style>
		.Edit_profile{
			display:none;
		}

		.Cad_profile{
			display:none;
		}
	</style>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="index.html"><img src="<?php echo base_url('Assets/gestao/img/logo-dark.png')?>" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>

				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
					
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url('Assets/gestao/img/user.png')?>" class="img-circle" alt="Avatar"> <span><?php echo $_SESSION['nome']?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo base_url('welcome/profile')?>"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
								<li><a href="<?php echo base_url('/')?>"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav><br>
					<ul class="nav">
						<li><a href="<?php echo base_url('welcome/inicio/1')?>" class=""><i class="lnr lnr-home"></i> <span>Home</span></a></li>
						<li><a href="<?php echo base_url('welcome/profile')?>" class="active"><i class="lnr lnr-file-empty"></i> <span>Profile</span></a></li>
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Cadastros Gerais</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="<?php echo base_url('welcome/clientes')?>" class="">Clientes</a></li>
									<li><a href="<?php echo base_url('welcome/levantamentos/1')?>" class="">Levantamentos</a></li>
								</ul>
							</div>
						</li>
						<li><a href="<?php echo base_url('welcome/LANDING_PAGE')?>" class=""><i class="lnr lnr-linearicons"></i> <span> LANDING PAGE</span></a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="panel panel-profile">
						<div class="clearfix">
							<!-- LEFT COLUMN -->
							<div class="profile-left">
								<!-- PROFILE HEADER -->
								<div class="profile-header">
									<div class="overlay"></div>
									<div class="profile-main">
									<?php foreach ($dados_usuario_logado as $info):?>
										<img src="<?php echo base_url('Assets/gestao/img/user-medium.png')?>" class="img-circle" alt="Avatar">
										<h3 class="name"><?php echo $info['nome']?></h3>
										<span class="online-status status-available">Available</span>
									</div>
									<div class="profile-stat">
										<div class="row">
											<div class="col-md-4 stat-item">
												45 <span>Projects</span>
											</div>
											<div class="col-md-4 stat-item">
												15 <span>Awards</span>
											</div>
											<div class="col-md-4 stat-item">
												2174 <span>Points</span>
											</div>
										</div>
									</div>
								</div>
								<!-- END PROFILE HEADER -->
								<!-- PROFILE DETAIL -->
								<div class="profile-detail">
									<div class="profile-info">
										
										<h4 class="heading">Basic Info</h4>
										<ul class="list-unstyled list-justify">
											<li>Name <span><?php echo $info['nome']?></span></li>
											<li>Email <span><?php echo $info['email']?></span></li>
											<li>Website <span>************</span></li>
										</ul><br><br>
										<?php endforeach;?>
									</div>
									<div class="profile-info">
										<h4 class="heading">Social</h4>
										<ul class="list-inline social-icons">
											<li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#" class="google-plus-bg"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="#" class="github-bg"><i class="fa fa-github"></i></a></li>
										</ul>
									</div>
									<div class="profile-info">
										<h4 class="heading">About</h4>
										<p>Interactively fashion excellent information after distinctive outsourcing.</p>
									</div>
									<div class="text-center col-sm-6 btn_Edit_profile"><a href="#" class="btn btn-primary">Editar Profile</a></div>
									<div class="text-center col-sm-6 btn_Cad_profile"><a href="#" class="btn btn-primary">Cadastrar User</a></div>
								</div><br><br>
								<!-- END PROFILE DETAIL -->
							</div>
							<!-- END LEFT COLUMN -->
							<!-- RIGHT COLUMN -->
							<div class="profile-right">								
								<!-- AWARDS -->
								<div class="awards">
									<div class="row">
										<div class="Edit_profile">
											<h4 class="heading col-sm-12">Atualizar dados de <?php echo $info['nome']?></h4>
											<form class="form" action="<?php echo base_url('welcome/profile')?>" method='POST'>
												<div class="form-group col-sm-12">
													<label for="email">Nome:  </label>
													<input type="text" class="form-control" name='nome'>
												</div>
												<div class="form-group col-sm-12">
													<label for="email">Email:  </label>
													<input type="email" class="form-control" name='email'>
												</div>
												<div class="form-group col-sm-12">
													<label for="pwd">Password:</label>
													<input type="password" class="form-control" name='password'>
												</div>
												<div class="form-group col-sm-12">
													<button type="submit" class="btn btn-default">Submit</button>
												</div>
												
											</form>
										</div>

										<div class="Cad_profile">
											<h4 class="heading col-sm-12">Cadastrar Novo Usuário</h4>
											<form class="form" action="<?php echo base_url('welcome/CreateAccount')?>"  method='POST'>
												<div class="form-group col-sm-12">
													<label for="email">Nome:  </label>
													<input type="text" class="form-control" name='nome'>
												</div>
												<div class="form-group col-sm-12">
													<label for="email">Email:  </label>
													<input type="email" class="form-control" name="email">
												</div>
												<div class="form-group col-sm-12">
													<label for="pwd">Password:</label>
													<input type="password" class="form-control"name='password'>
												</div>
												<div class="form-group col-sm-12">
													<button type="submit" class="btn btn-default">Submit</button>
												</div>
											</form>
										</div>
									</div>
								</div>
								<!-- END AWARDS -->
								<!-- TABBED CONTENT -->
								<div class="custom-tabs-line tabs-line-bottom left-aligned">
									<ul class="nav" role="tablist">
										<p class='col-sm-12'><?php echo $Mensagem?></p>									
										<li><a href="#tab-bottom-left2" role="tab" data-toggle="tab">Usuários <span class="badge"><?php echo $count_users;?></span></a></li>
									</ul>
								</div>
								<div class="tab-content">
									
									<div id="tab-bottom-left2">
										<div class="table-responsive">
											<table class="table project-table">
												<thead>
													<tr>
														<th>#</th>
														<th>Nome</th>
														<th>E-mail</th>
														<th>Delete</th>														
														<th>Atualizar</th>
													</tr>
												</thead>
												<tbody>
													<?php foreach($dados_usuarios_geral as $info2):?>
													<tr>
														<td><?php echo $info2['id_usuario']?></td>
														<td><a href="#"><img src="<?php echo base_url('Assets/gestao/img/user2.png')?>" alt="Avatar" class="avatar img-circle"> <a href="#"><?php echo $info2['nome']?></a></td>
														<td><?php echo $info2['email']?></a></td>														
														<td><form action="<?php echo base_url('welcome/profile')?>" method='POST'><button class='btn btn-success btn-xs' name='id_user_delete' value='<?php echo $info2['id_usuario']?>'>DELETE</button></form></td>
														<td><button class='btn btn-success btn-xs id_user_altera' name='id_user_altera' value='<?php echo $info2['id_usuario']?>'>ATUALIZAR</button></td>
													</tr>
													
												</tbody><br>
											<?php endforeach;?>
											</table><br><br><br><br>
										</div>
									</div>
								</div>
								<!-- <p class='teste'><?php echo $dados_id?></p> -->
								<!-- END TABBED CONTENT -->
							</div>
							<!-- END RIGHT COLUMN -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div><br><br>
		<footer>
			<div class="container-fluid">
				<p class="copyright bg-info col-sm-12">&copy; 2017 <a href="https://www.themeineed.com" target="_blank">Theme I Need</a>. All Rights Reserved.</p>
			</div>
		</footer>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<?php echo base_url('Assets/gestao/vendor/jquery/jquery.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/vendor/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/scripts/klorofil-common.js')?>"></script>
</body>

</html>

<script>
	$(function() { 
		$('.btn_Edit_profile').click(function() {
			$('.Cad_profile').css('display', 'none');
			$('.Edit_profile').css('display', 'block');			
		})

		$('.btn_Cad_profile').click(function() {
			$('.Edit_profile').css('display', 'none');	
			$('.Cad_profile').css('display', 'block');
		})

		$('.id_user_altera').click(function(data) {
			var id_user_altera = $('.id_user_altera').val();
						
			$.post('profile', {id: id_user_altera}, function(result) {
				console.log(result);
				$('.teste').html(result);
			})
		})
		
	});
</script>