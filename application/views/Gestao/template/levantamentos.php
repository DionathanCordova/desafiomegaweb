<!doctype html>
<html lang="en">

<head>
	<title>Levantamentos</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/vendor/bootstrap/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/vendor/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/vendor/linearicons/style.css')?>">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/css/main.css')?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/css/demo.css')?>">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('Assets/gestao/img/apple-icon.png')?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('Assets/gestao/img/favicon.png')?>">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="index.html"><img src="<?php echo base_url('Assets/gestao/img/logo-dark.png')?>" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">					
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url('Assets/gestao/img/user.png')?>" class="img-circle" alt="Avatar"> <span><?php echo $_SESSION['nome']?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo base_url('welcome/profile')?>"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
								<li><a href="<?php echo base_url('/')?>"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav><br>
					<ul class="nav">
						<li><a href="<?php echo base_url('welcome/inicio/1')?>" class=""><i class="lnr lnr-home"></i> <span>Home</span></a></li>
						<li><a href="<?php echo base_url('welcome/profile')?>" class="" ><i class="lnr lnr-file-empty"></i> <span>Profile</span></a></li>
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Cadastros Gerais</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="<?php echo base_url('welcome/clientes')?>" class="">Clientes</a></li>
									<li><a href="<?php echo base_url('welcome/levantamentos/1')?>" class="">Levantamentos</a></li>
								</ul>
							</div>
						</li>
						<li><a href="<?php echo base_url('welcome/LANDING_PAGE')?>" class="" ><i class="lnr lnr-linearicons"></i> <span> LANDING PAGE</span></a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<h3 class="page-title">Tables</h3>
					<div class="row">

                        <div class="col-md-6">
							<!-- Cadastro de Luminárias-->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Cadastro de Levantamento</h3>
								</div>

								<form action="<?php echo base_url('welcome/insert_levantamento')?>" method='POST'>
									<div class='form-group container-fluid'>
                                        <div class="form-group">
                                            <label for="nome">Selecione o Cliente</label>
                                            <select name='id_cliente' class='form-control' id='nome' required>
                                                <option value="" class="form-control"></option>
                                                <?php foreach ($dados_clientes as $info):?> 
                                                <option value="<?php echo $info['id_cliente']?>" class="form-control"><?php echo $info['nome']?></option>
                                                <?php endforeach?>
                                            </select>
                                        </div>
                                      
                                       
                                        <label for="nome">Descrição do Levantamento</label>
                                        <input type="text" name="descricao" class='form-control' required><br>
                                    
                                        
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 55px; height: 55px;"></div>
                                                <div>
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="..."></span><br>   
                                                </div>
                                            </div><br>
                                            <div class='col-sm-4'>
                                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div>
                                            <div class='col-sm-2'>
                                                <button class='btn btn-default btn_qtd'>Enviar</button>		
                                            </div>						
									    </div>
                                </form>
                                    
                                

							</div>
							<!-- Cadastro de Luminárias -->
						</div>

						<div class="col-md-6">
							<!-- Cadastro de Blocos -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Cadastro de Blocos</h3>
								</div>

                                <form action="" method='POST'>
                                   
                                    <div class='form-group container-fluid'>        
										<label for="nome">Qtd de Blocos</label>
										<input type="numer" name='qtd_blocos' class='form-control qtd_blocos' required><br>
                                        <button class='btn btn-default btn_qtd'>Adicionar</button>
                                    </div><br><br><br><br><br>
                                </form>

                   
								<form action="<?php echo base_url('welcome/insert_blocos')?>" method='POST'>
									<div class='form-group container-fluid'>
                                        <div class="form-group">
                                            <label for="nome">Selecione um dos levantamentos Ativos</label>
                                            <select name='id_levantamento' class='form-control' id='nome' required>
                                                <option value="" class="form-control"></option>
                                                <?php foreach ($dados_selecao_cliente as $info):var_dump($dados_selecao_cliente )?> 
                                                <option value="<?php echo $info['id_levantamento']?>" class="form-control" required><?php echo $info['nome'] . ' - ' . $info['descricao']?></option>
                                                <?php endforeach?>
                                            </select>
                                        </div>
                                        
                                        <?php if(isset($_POST['qtd_blocos'])) {
                                            $qtd_blocos = $_POST['qtd_blocos'];
                                        }else{
                                            $qtd_blocos = 0;
                                        };?>
                                        <?php for ($i=0; $i < $qtd_blocos; $i++):?>
                                            <label for="nome">Descrição do Bloco</label>
                                            <input type="text" name="descricao<?php echo $i?>" class='form-control' required><br>
                                        <?php endfor?>	
                                        <button class='btn btn-default btn_qtd'>Enviar</button>									
									</div>
                                    
								</form>

							</div>
							<!-- Cadastro de Blocos -->
						</div>

                        <div class="col-md-6">
							<!-- Cadastro de Pavimentos-->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Cadastro de Pavimentos</h3>
								</div>

                                <form action="" method='POST'>
                                   
                                    <div class='form-group container-fluid'>        
										<label for="nome">Qtd de Pavimentos</label>
										<input type="numer" name='qtd_pavimentos' class='form-control qtd_blocos' required><br>
                                        <button class='btn btn-default btn_qtd'>Adicionar</button>
                                    </div>
								</form><br><br>


                   
								<form action="<?php echo base_url('welcome/insert_pavimentos')?>" method='POST'>
									<div class='form-group container-fluid'>
                                        <div class="form-group">
                                            <label for="nome">Selecione o Cliente</label>
                                            <select name='id_levantamento' class='form-control' id='nome' required>
                                                <option value="" class="form-control"></option>
                                                <?php foreach ($dados_selecao_cliente as $info):var_dump($dados_selecao_cliente )?> 
                                                <option value="<?php echo $info['id_levantamento']?>" class="form-control" required><?php echo $info['nome'] . ' - ' . $info['descricao']?></option>
                                                <?php endforeach?>
                                            </select>
                                        </div>
                                        
                                        <?php if(isset($_POST['qtd_pavimentos'])) {
                                            $qtd_pavimentos = $_POST['qtd_pavimentos'];
                                        }else{
                                            $qtd_pavimentos = 0;
                                        };?>
                                        <?php for ($i=0; $i < $qtd_pavimentos; $i++):?>
                                            <label for="nome">Descrição do Bloco</label>
                                            <input type="text" name="descricao<?php echo $i?>" class='form-control' required><br>
                                        <?php endfor?>	
                                        <button class='btn btn-default btn_qtd'>Enviar</button>									
									</div>
                                    
								</form>

							</div>
							<!-- Cadastro de Pavimentos -->
						</div>

                        <div class="col-md-6">
							<!-- Cadastro de Luminárias-->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Cadastro de Luminárias</h3>
								</div>

                                <form action="" method='POST'>
                                   
                                    <div class='form-group container-fluid'>        
										<label for="nome">Qtd de Luminárias</label>
										<input type="numer" name='qtd_luminarias' class='form-control qtd_blocos' required><br>
                                        <button class='btn btn-default btn_qtd'>Adicionar</button>
                                    </div>
								</form><br><br>


                   
								<form action="<?php echo base_url('welcome/insert_luminarias')?>" method='POST'>
									<div class='form-group container-fluid'>
                                        <div class="form-group">
                                            <label for="nome">Selecione o Cliente</label>
                                            <select name='id_levantamento' class='form-control' id='nome' required>
                                                <option value="" class="form-control"></option>
                                                <?php foreach ($dados_selecao_cliente as $info):var_dump($dados_selecao_cliente )?> 
                                                <option value="<?php echo $info['id_levantamento']?>" class="form-control" required><?php echo $info['nome'] . ' - ' . $info['descricao']?></option>
                                                <?php endforeach?>
                                            </select>
                                        </div>
                                        
                                        <?php if(isset($_POST['qtd_luminarias'])) {
                                            $qtd_luminarias = $_POST['qtd_luminarias'];
                                        }else{
                                            $qtd_luminarias = 0;
                                        };?>
                                        <?php for ($i=0; $i < $qtd_luminarias; $i++):?>
                                            <label for="nome">Descrição do Bloco</label>
                                            <input type="text" name="descricao<?php echo $i?>" class='form-control' required><br>
                                        <?php endfor?>	
                                        <button class='btn btn-default btn_qtd'>Enviar</button>									
									</div>
                                    
								</form>

							</div>
							<!-- Cadastro de Luminárias -->
						</div>



						<div class="col-md-12">
							<!-- TABLE NO PADDING -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title text-center">Levantamentos em Andamento</h3>
								</div>

                                <form action="<?php echo base_url('welcome/levantamentos/1')?>" method='POST'>
									<div class="form-group  col-sm-4">
										<label for="nome">Selecione o Cliente</label>
										<select name='id_cliente' class='form-control' id='nome'>
                                            <option value="" class="form-control"></option>
											<?php foreach ($dados_clientes as $info):?> 
											<option value="<?php echo $info['id_cliente']?>" class="form-control select_cliente "><?php echo $info['nome']?></option>
											<?php endforeach?>
										</select>										
									</div>
                                    <div class="form-group col-sm-12">
                                        <button type="submit" class="btn btn-default btn-">Pesquisar</button>
                                    </div>
                                </form>
								<div class="panel-body no-padding">
									<table class="table">
										<thead>
											<tr >
                                                <th class='text-center'>#</th>
												<th class='text-center'>Descrição do Levantamento</th>
												<th class='text-center'>Cliente</th>
                                                <th class='text-center'>Colaborador</th>
												<th class='text-center'>Descrição dos Blocos</th>
												<th class='text-center'>Descricao dos Pavimentos</th>
												<th class='text-center'>Descricao dos Luminarias</th>
                                                <th class='text-center'>status/th>
                                                <th class='text-center'>Avaliação Aceita</th>
                                                <th class='text-center'>Avaliação Rejeitada</th>
                                                <th class='text-center'>Cancelar</th>
											</tr>
										</thead>
										<tbody>
                                        <?php if($dados_levantamento != NULL): ?>
										<?php foreach($dados_levantamento as $info):?>
											<tr>
                                                <th class='text-center'><?php echo $info['id_Luminarias']?></th>
												<th class='text-center'><?php echo $info['Descricao_Levantamento']?></th>
												<th class='text-center'><?php echo $info['Cliente']?></th>
												<th class='text-center'><?php echo $info['Colaborador']?></th>
												<th class='text-center'><?php echo $info['Descricao_Blocos']?></th>
												<th class='text-center'><?php echo $info['Descricao_Pavimentos']?></th>
												<th class='text-center'><?php echo $info['Descricao_Luminarias']?></th>
                                                <th class='text-center'><?php echo $info['status']?></th>
                                                <?php if($info['status'] != 'Avaliar'):?>
                                                <th class='text-center'><button class='btn btn-default btn-xs'  name='btn_status_ok'><?php echo $info['status']?></button> </th>
                                                <th class='text-center'><button class='btn btn-default btn-xs'  name='btn_status_fail'><?php echo $info['status']?></button></th>
                                                <?php else:?>
                                                <th class='text-center'><form action="<?php echo base_url('welcome/levantamentos/1')?>" method='POST'><button class='btn btn-info btn-xs' value='<?php echo $info['id_levantamento']. '_'.$info['id_Luminarias']?>' name='btn_status_ok'>Status Aceito</button> </form></th>
                                                <th class='text-center'><form action="<?php echo base_url('welcome/levantamentos/1')?>" method='POST'><button class='btn btn-danger btn-xs' value='<?php echo $info['id_levantamento']. '_'.$info['id_Luminarias']?>' name='btn_status_fail'>Status Rejeitado</button></form></th>
                                                <?php endif?>

                                                <th class='text-center'><form action="<?php echo base_url('welcome/levantamentos/1')?>" method='POST'><button class='btn btn-success btn-xs' value='<?php echo $info['id_levantamento']?>' name='btn_status_cancel'>Deletar</button></form></th>
                                             
                                                
                                            
                                            </tr>
										<?php endforeach?>
                                        <?php endif?>
										</tbody>
									</table>
								</div>
                                <div class="paginacao col-sm-12 col-xs-12">
											<?php        
												if(isset($pagina)) {
													$p = $pagina;
												}else{
													$p = 0;
												}

												$_SESSION['p'] = 0;
												if($p >= 0) {
													$anterior = $p - 1;
													$_SESSION['p'] = $anterior;
												}
												if($p <= $count) {
													$proxima = $p + 1;
													$_SESSION['p'] = $proxima;
												}
												
												if($anterior <= 0) {
													$anterior = 0;
												}
												if(isset($proxima) && $proxima >= $count){
													$proxima = $count;
												}

												if($count > $total_registros):?>
													<ul class="pagination">
														<?php if($p > 1):?>
															<li><a class='pagina' href="<?php echo base_url('welcome/levantamentos/' .$anterior );?>"><</a></li>
															<li><a class='pagina' href="<?php echo base_url('welcome/levantamentos/' .$anterior );?>"><?=$anterior;?></a></li>
														<?php endif?>

														<li><a class='pagina active' href="<?php echo base_url('welcome/levantamentos/') .$p;?>"><strong class='active'><?=$p;?></strong></a></li>

														<?php if($pHome+8 <= $count):?>
															<li><a class='pagina' href="<?php echo base_url('welcome/levantamentos/' .$proxima);?>"><?=$proxima;?></a></li>
															<li><a class='pagina' href="<?php echo base_url('welcome/levantamentos/' .$proxima);?>">></a></li>
														<?php endif?>
													</ul>
											<?php endif;?>
										</div>
							</div>
							<!-- END TABLE NO PADDING -->
						</div>
					</div>					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">&copy; 2017 <a href="https://www.themeineed.com" target="_blank">Theme I Need</a>. All Rights Reserved.</p>
			</div>
		</footer>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<?php echo base_url('Assets/gestao/vendor/jquery/jquery.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/vendor/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/scripts/klorofil-common.js')?>"></script>

    
<script>
	$(function() { 
		// $('.btn_qtd').click(function() {
           
        //     var qtd_blocos = $('.qtd_blocos').val();
        //     alert(qtd_blocos);


		// })
		
	});
</script>
</body>

</html>
