<!doctype html>
<html lang="en">

<head>
	<title>Desafio MegaWeb</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/vendor/bootstrap/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/vendor/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/vendor/linearicons/style.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/vendor/chartist/css/chartist-custom.css')?>">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/css/main.css')?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?php echo base_url('Assets/gestao/css/demo.css')?>">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('Assets/gestao/img/apple-icon.png')?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('Assets/gestao/img/favicon.png')?>">
	<style>
		.projecao{
			height:43.5vmin;
		}

		.select_cliente{
			width:10vmin;
		}
	</style>
</head>


<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="index.html"><img src="<?php echo base_url('Assets/gestao/img/logo-dark.png')?>" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>

				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
					
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url('Assets/gestao/img/user.png')?>" class="img-circle" alt="Avatar"> <span><?php echo $_SESSION['nome']?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo base_url('welcome/profile')?>"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
								<li><a href="<?php echo base_url('/')?>"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav><br>
					<ul class="nav">
						<li><a href="<?php echo base_url('welcome/inicio/1')?>" class="active"><i class="lnr lnr-home"></i> <span>Home</span></a></li>
						<li><a href="<?php echo base_url('welcome/profile')?>" class=""><i class="lnr lnr-file-empty"></i> <span>Profile</span></a></li>					
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Cadastros Gerais</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="<?php echo base_url('welcome/clientes')?>" class="">Clientes</a></li>
									<li><a href="<?php echo base_url('welcome/levantamentos/1')?>" class="">Levantamentos</a></li>
								</ul>
							</div>
						</li>
						<li><a href="<?php echo base_url('welcome/LANDING_PAGE')?>" class=""><i class="lnr lnr-linearicons"></i> <span> LANDING PAGE</span></a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					
					<!-- END OVERVIEW -->
					<div class="row">						
						<div class="col-sm-6">
							<!-- MULTI CHARTS -->
							<div class="panel projecao contailer">
								<div class="panel-heading">
									<h3 class="panel-title">Projeção de Clientes Agregados a Nossa Empresa</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body">
									<div id="visits-trends-chart" class="ct-chart"></div>
								</div><br><br><br>
							</div>
							<!-- END MULTI CHARTS -->
						</div>

						<div class="col-sm-6">
							<!-- TASKS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Nossos Clientes - Comparativos de Qualidade de seus Equipamentos</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body">
									<ul class="list-unstyled task-list">
										<?php foreach($dados_status as $info):?>
										<li>										
											<p><?php echo $info['cliente']?><span class="label-percent"> <?php echo $info['count_positivo'] . '%'?></span></p>
											<div class="progress progress-xs">
												<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $info['count_positivo']. '%' ?>">>
													<span class="sr-only">80% Complete</span>
												</div>
											</div>
										</li>
										
										<!-- <li>
											<p>Data Duplication Check <span class="label-percent">100%</span></p>
											<div class="progress progress-xs">
												<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
													<span class="sr-only">Success</span>
												</div>
											</div>
										</li>
										<li>
											<p>Server Check <span class="label-percent">45%</span></p>
											<div class="progress progress-xs">
												<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
													<span class="sr-only">45% Complete</span>
												</div>
											</div>
										</li>
										<li>
											<p>Mobile App Development <span class="label-percent">10%</span></p>
											<div class="progress progress-xs">
												<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
													<span class="sr-only">10% Complete</span>
												</div>
											</div>
										</li> -->
										<?php endforeach?>
									</ul>
								</div>
							</div>
							<!-- END TASKS -->
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">

								<div class="panel-heading">
									<h3 class="panel-title text-center col-sm-12">Recent Purchases</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
									
									<form action="<?php echo base_url('welcome/inicio/1')?>" method='POST'>
									<div class="form-group  col-sm-4">
										<label for="nome">Selecione o Cliente</label>
										<select name='id_cliente' class='form-control' id='nome'>
											<?php foreach ($dados_clientes as $info):?> 
											<option value="<?php echo $info['id_cliente']?>" class="form-control select_cliente "><?php echo $info['nome']?></option>
											<?php endforeach?>
										</select>
										
									</div>
										<div class="form-group col-sm-12">
											<button type="submit" class="btn btn-default btn-">Pesquisar</button>
										</div>
									</form>
								</div>
								<div class="panel-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th class='text-center'>Descricao Levantamento</th>
												<th class='text-center'>Cliente</th>
												<th class='text-center'>Colaborador</th>
												<th class='text-center'>Descricao Blocos</th>
												<th class='text-center'>Descricao_Pavimentos</th>
												<th class='text-center'>Status</th>
											</tr>
										</thead>
										<tbody>
										<?php if($dados_levantamento != NULL): ?>
											<?php foreach($dados_levantamento as $info):?>
											<tr>
												<td class='text-center'><?php echo $info['Descricao_Levantamento']?></td>
												<td class='text-center'><?php echo $info['Cliente']?></td>
												<td class='text-center'><?php echo $info['Colaborador']?></td>
												<td class='text-center'><?php echo $info['Descricao_Blocos']?></td>
												<td class='text-center'><?php echo $info['status']?></td>
												<?php if($info['status'] == 'Status ruim'):?>
												<td class='text-center'><span class="label label-warning"><?php echo $info['status']?></span></td>
												<?php elseif($info['status'] == 'Status bom'):?>
												<td class='text-center'><span class="label label-info"><?php echo $info['status']?></span></td>
												<?php else:?>
												<td class='text-center'><span class="label label-success">Avaliar</span></td>
												<?php endif?>
											
											</tr>
											<?php endforeach?>
											<?php endif?>
										</tbody>
									</table> 
								</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>
										<div class="paginacao col-sm-12 col-xs-12">
											<?php        
												if(isset($pagina)) {
													$p = $pagina;
												}else{
													$p = 0;
												}

												$_SESSION['p'] = 0;
												if($p >= 0) {
													$anterior = $p - 1;
													$_SESSION['p'] = $anterior;
												}
												if($p <= $count) {
													$proxima = $p + 1;
													$_SESSION['p'] = $proxima;
												}
												
												if($anterior <= 0) {
													$anterior = 0;
												}
												if(isset($proxima) && $proxima >= $count){
													$proxima = $count;
												}

												if($count > $total_registros):?>
													<ul class="pagination">
														<?php if($p > 1):?>
															<li><a class='pagina' href="<?php echo base_url('welcome/inicio/' .$anterior);?>"><</a></li>
															<li><a class='pagina' href="<?php echo base_url('welcome/inicio/' .$anterior);?>"><?=$anterior;?></a></li>
														<?php endif?>

														<li><a class='pagina active' href="<?php echo base_url('welcome/inicio/') .$p;?>"><strong class='active'><?=$p;?></strong></a></li>

														<?php if($pHome+8 <= $count):?>
															<li><a class='pagina' href="<?php echo base_url('welcome/inicio/' .$proxima);?>"><?=$proxima;?></a></li>
															<li><a class='pagina' href="<?php echo base_url('welcome/inicio/' .$proxima);?>">></a></li>
														<?php endif?>
													</ul>
											<?php endif;?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">&copy; 2017 <a href="https://www.themeineed.com" target="_blank">Theme I Need</a>. All Rights Reserved.</p>
			</div>
		</footer>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<?php echo base_url('Assets/gestao/vendor/jquery/jquery.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/vendor/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/vendor/chartist/js/chartist.min.js')?>"></script>
	<script src="<?php echo base_url('Assets/gestao/scripts/klorofil-common.js')?>"></script>
	<script>
	$(function() {
		var data, options;

		// headline charts
		data = {
			labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
			series: [
				[23, 29, 24, 40, 25, 24, 35],
				[14, 25, 18, 34, 29, 38, 44],
			]
		};

		options = {
			height: 300,
			showArea: true,
			showLine: false,
			showPoint: false,
			fullWidth: true,
			axisX: {
				showGrid: false
			},
			lineSmooth: false,
		};

		new Chartist.Line('#headline-chart', data, options);


		// visits trend charts
		data = {
			labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			series: [{
				name: 'series-real',
				data: [200, 380, 350, 320, 410, 450, 570, 400, 555, 620, 750, 900],
			}, {
				name: 'series-projection',
				data: [240, 350, 360, 380, 400, 450, 480, 523, 555, 600, 700, 800],
			}]
		};

		options = {
			fullWidth: true,
			lineSmooth: false,
			height: "270px",
			low: 0,
			high: 'auto',
			series: {
				'series-projection': {
					showArea: true,
					showPoint: false,
					showLine: false
				},
			},
			axisX: {
				showGrid: false,

			},
			axisY: {
				showGrid: false,
				onlyInteger: true,
				offset: 0,
			},
			chartPadding: {
				left: 20,
				right: 20
			}
		};

		new Chartist.Line('#visits-trends-chart', data, options);


		// visits chart
		data = {
			labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
			series: [
				[6384, 6342, 5437, 2764, 3958, 5068, 7654]
			]
		};

		options = {
			height: 300,
			axisX: {
				showGrid: false
			},
		};

		new Chartist.Bar('#visits-chart', data, options);


		// real-time pie chart
		var sysLoad = $('#system-load').easyPieChart({
			size: 130,
			barColor: function(percent) {
				return "rgb(" + Math.round(200 * percent / 100) + ", " + Math.round(200 * (1.1 - percent / 100)) + ", 0)";
			},
			trackColor: 'rgba(245, 245, 245, 0.8)',
			scaleColor: false,
			lineWidth: 5,
			lineCap: "square",
			animate: 800
		});

		var updateInterval = 3000; // in milliseconds

		setInterval(function() {
			var randomVal;
			randomVal = getRandomInt(0, 100);

			sysLoad.data('easyPieChart').update(randomVal);
			sysLoad.find('.percent').text(randomVal);
		}, updateInterval);

		function getRandomInt(min, max) {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}

	});
	</script>
</body>

</html>
