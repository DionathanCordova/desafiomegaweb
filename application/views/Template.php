<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Elastic Image Slideshow with Thumbnail Preview</title>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Elastic Image Slideshow with Thumbnail Preview" />
        <meta name="keywords" content="jquery, css3, responsive, image, slider, slideshow, thumbnails, preview, elastic" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('Assets/inicio/css/demo.css')?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('Assets/inicio/css/style.css')?>" />
        <link rel="stylesheet" href="<?php echo base_url('/Assets/css/bootstrap.min.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('Assets/footer/css/Footer-with-logo.css')?>">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic' rel='stylesheet' type='text/css' />
		<noscript>
			<link rel="stylesheet" type="text/css" href="<?php echo base_url('Assets/inicio/css/noscript.css')?>" />
        </noscript>

        <script src="<?php echo base_url('Assets/js/library/jquery.js')?>"></script>
        <script src="<?php echo base_url('Assets/js/bootstrap.min.js')?>"></script>
   </head>
    <body>

        <div>
            <div class="header" style='height:5vmin;font-size:1.5vmin; padding-top:1vmin;'>
                <a href="<?php echo base_url('welcome/Quem_somos')?>">
                    <strong>&laquo; QUEM SOMOS</strong>
                </a>
                <span class="right">
                    <a href="<?php echo base_url('welcome/Produtos_servicos')?>">PRODUTOS E SERVIÇOS</a>
                    <a href="<?php echo base_url('/')?>">
                        <strong>ACESSO</strong>
                    </a>
                </span>
                <div class="clr"></div>
            </div>
            <h1></h1>
            <div class="wrapper">
                <div id="ei-slider" class="ei-slider">
                    <ul class="ei-slider-large">
                        <li>
                            <img src="<?php echo base_url('Assets/inicio/images/large/LUM1.png" alt="image01')?>" />
                            <div class="ei-title">
                                <h2>Creative</h2>
                                <h3>Duet</h3>
                            </div>
                        </li>
                        <li>
                            <img src="<?php echo base_url('Assets/inicio/images/large/LUM2.png" alt="image02')?>" />
                            <div class="ei-title">
                                <h2>Friendly</h2>
                                <h3>Devil</h3>
                            </div>
                        </li>
                        <li>
                            <img src="<?php echo base_url('Assets/inicio/images/large/LUM3.png" alt="image03')?>"/>
                            <div class="ei-title">
                                <h2>Tranquilent</h2>
                                <h3>Compatriot</h3>
                            </div>
                        </li>
                        <li>
                            <img src="<?php echo base_url('Assets/inicio/images/large/LUM4.png" alt="image04')?>"/>
                            <div class="ei-title">
                                <h2>Insecure</h2>
                                <h3>Hussler</h3>
                            </div>
                        </li>
                      
                    </ul><!-- ei-slider-large -->
                    <ul class="ei-slider-thumbs">
                        <li class="ei-slider-element">Current</li>
                        <li><a href="#">Slide 1</a><img src="<?php echo base_url('Assets/inicio/images/thumbs/LUM1.png')?>" alt="thumb01" /></li>
                        <li><a href="#">Slide 2</a><img src="<?php echo base_url('Assets/inicio/images/thumbs/LUM2.png')?>" alt="thumb02" /></li>
                        <li><a href="#">Slide 3</a><img src="<?php echo base_url('Assets/inicio/images/thumbs/LUM3.png')?>" alt="thumb03" /></li>
                        <li><a href="#">Slide 4</a><img src="<?php echo base_url('Assets/inicio/images/thumbs/LUM4.png')?>" alt="thumb04" /></li>
                    </ul><!-- ei-slider-thumbs -->
                </div><!-- ei-slider -->
                <div class="reference">
					<p class="demos"> <?php $this->load->view($viewName);?></p>
                </div>
            </div><!-- wrapper -->
        </div>
        <script type="text/javascript" src="<?php echo base_url('Assets/inicio/js/jquery.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('Assets/inicio/js/jquery.eislideshow.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('Assets/inicio/js/jquery.easing.1.3.js')?>"></script>
        <script type="text/javascript">
            $(function() {
                $('#ei-slider').eislideshow({
					easing		: 'easeOutExpo',
					titleeasing	: 'easeOutExpo',
					titlespeed	: 1200
                });
            });
        </script>
    </body><br><br>


<footer id="myFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h5>Get started</h5>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Sign up</a></li>
                        <li><a href="#">Downloads</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>About us</h5>
                    <ul>
                        <li><a href="#">Company Information</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Reviews</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>Support</h5>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Help desk</a></li>
                        <li><a href="#">Forums</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 info">
                    <h5>Information</h5>
                    <p> Lorem ipsum dolor amet, consectetur adipiscing elit. Etiam consectetur aliquet aliquet. Interdum et malesuada fames ac ante ipsum primis in faucibus. </p>
                </div>
            </div>
        </div>
        <div class="second-bar">
           <div class="container">
                <h2 class="logo"><a href="#"> LOGO </a></h2>
                <div class="social-icons">
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                </div>
            </div>
        </div>
    </footer>


</html>